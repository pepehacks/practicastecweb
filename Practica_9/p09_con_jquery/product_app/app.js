// JSON BASE A MOSTRAR EN FORMULARIO
$(document).ready(function(){
    var baseJSON = {
        "precio": 0.0,
        "unidades": 1,
        "modelo": "XX-000",
        "marca": "NA",
        "detalles": "NA",
        "imagen": "img/default.png"
    };
    
    

    function init() {
        /**
         * Convierte el JSON a string para poder mostrarlo
         * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
         */
        var JsonString = JSON.stringify(baseJSON,null,2);
        document.getElementById("description").value = JsonString;
    
        // SE LISTAN TODOS LOS PRODUCTOS
        listarProductos();
    }
    
    console.log('Esta jalando');
    
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;

    $('#search').keyup(function(e){
        let search = $('#search').val();
        console.log(search);
        $.ajax({
            url: 'backend/product-search.php',
            type: 'POST',
            data: { search },
            success: function(response){
                let product = JSON.parse(response);
                let template = '';
                product.forEach(product => {

                    template += `<li> ${product.nombre} </li>`
                    
                });
                $('#container').html(template);
                
            }
        })
    })
    $('#product-form').submit(function(e){
        
        var JsonString = JSON.stringify(baseJSON,null,2);
        document.getElementById("description").value = JsonString;
        var productoJsonString = document.getElementById('description').value;
        var finalJSON = JSON.parse(productoJsonString);
        console.log(finalJSON);

        $.post('backend/product-add.php',finalJSON,function (response){
            console.log(response);
        });
        e.preventDefault();
        

        
    });
    $.ajax({
        url: 'backend/product-list.php',
        type: 'GET',
        success: function (response){
            let productos = JSON.parse(response);
            let template = '';
            productos.forEach(producto => {
                template += `
                    <tr productId=${producto.id}>
                        <td> ${producto.id} </td>
                        <td> ${producto.nombre} </td>
                        <td> ${producto.detalles} </td>
                        <td> <button class="product-delete btn btn-danger"> Delete </button> </td>
                    </tr>
                `
            });
            $('#products').html(template);


            
        }

    })

    $(document).on('click', '.product-delete', function(){
        if(confirm("Confirmacion para eliminar el prducto seleccionado")){
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            
            $.post('backend/product-delete.php',{id}, function(response){
                console.log(response);
        });
        }

    })
});
