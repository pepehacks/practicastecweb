<?php
    namespace BaseDatos;
    abstract class DataBase{
        
        protected $conexion;

        public function __construct($name_bd){
            $this->conexion = @mysqli_connect('localhost','root','114311',$name_bd);
        }

        public function getConexion(){
            if(!$this->conexion) {
                die('¡Base de datos NO conectada!');
            }
            return $this->conexion;
        }
    }

?>