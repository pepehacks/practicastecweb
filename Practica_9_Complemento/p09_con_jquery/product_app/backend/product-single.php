<?php
    include('database.php');
    $id = $_POST['id'];
    $sql = "SELECT * FROM productos WHERE id = $id";

    $result = mysqli_query($conexion,$sql);

    if(!$result){
        die("Query failed");
    }

    $json = array();
    while($row = mysqli_fetch_array($result)){
        $json[] = array(
            'nombre' => $row['nombre'],
            'detalles' => $row['detalles'],
            'precio' => $row['precio'],
            'marca' => $row['marca'],
            'modelo' => $row['modelo'],
            'unidades' => $row['unidades']

        );
    }
    $jsonString = json_encode($json[0]);
    echo $jsonString;
?>