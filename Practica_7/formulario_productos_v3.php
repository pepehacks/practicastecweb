<!DOCTYPE html>
<html>
<style type="text/css">
    li { 
      list-style-type: none;
      padding-bottom: 8px;
      }
    </style>
    <form action="set_productov2.php" method="post" onsubmit="return validar()">
    <div>
    <ul>
          
          <li><label>Nombre:</label><br><input type="text" name="nombre" id="producto_nombre" require></li>
          <li><label>Marca:</label><br>
          <select id="marca" name="marca">
            <option value="Sony">Sony</option>
            <option value="Xbox">Xbox</option>
            <option value="Nintendo">Nintendo</option>
          </select></li>
        </li>
          <li><label>Modelo:</label><br><input type="text" name="modelo" id="producto_modelo" require></li>
          <li><label>Precio:</label><br><input type="number" step="0.01" name="precio" id="producto_precio" require></li>
          <li><label>Detalles:</label><br><input type="text" name="detalles" id="producto_detalles" require></li>
          <li><label>Unidades:</label><br><input type="number" name="unidades" id="producto_unidades" require></li>
          <!-- <li><label>Imagen:</label><br><input type="file" name="imagen" id="producto_imagen"></textarea></li> -->
    </ul>
    </div>
        <input type="submit" value="Registrar producto">
        <input type="reset" value="Limpiar">
    </form>

</html>


<script type="text/javascript">
      function validar()
      {
        valor = true;
        var nombre = document.getElementById('producto_nombre').value;
        if (!nombre) {
          alert("Falta el campo de nombre");
          valor = false;
        }if(nombre.length > 100)
          {
            alert("El nombre es demaciado largo")
            valor = false;
          }
        
          var modelo = document.getElementById('producto_modelo').value;
        if (!modelo) {
          alert("Falta el campo de modelo");
          valor = false;
        }if(modelo.length > 25)
          {
            alert("El nombre del modelo es demaciado largo")
            valor = false;
          }
        
          var precio = document.getElementById('producto_precio').value;
        if (!precio) {
          alert("Falta el campo de precio");
          valor = false;
        }if(precio  <= 99.99)
          {
            alert("El precio debe ser mayor de 99.99")
            valor = false;
          }
        
          var detalles = document.getElementById('producto_detalles').value;
        if( detalles && detalles.length > 250)
          {
            alert("Los detalles deben describirse en menos de 250 caracteres")
            valor = false;
          }

          var unidades = document.getElementById('producto_unidades').value;
        if (!unidades) {
          alert("Falta el campo de unidades");
          valor = false;
        }if(unidades < 0)
          {
            alert("No puedes ingresar unidades negativas")
            valor = false;
          }
          return valor;
      }
</script>